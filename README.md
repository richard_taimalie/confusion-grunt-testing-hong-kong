# README #

### What is this repository for? ###

Completed course material from the Front End Javascript module as part of the Full stack web development course.

Welcome to Front-End JavaScript Frameworks: AngularJS course. This course is the third in the series of courses that form part of the Full-Stack Web Development Specialization. This course will give you an overview of client-side JavaScript frameworks, in particular AngularJS. You will learn about AngularJS, the model-view-controller framework, Angular directives, filters, and controllers. You will design a single page application using AngularJS. You will learn about client-server communication between your Angular application and a RESTful server. You will also learn about Web tools like Grunt, Gulp and Yo, and Yeoman workflow.

At the end of this course, you will be able to:

Set up, design and style a single page application using AngularJS
Enable client-server communication between your Angular application and a RESTful server
Make use of web tools to setup and manage your Angular application.
This course includes four modules, each corresponding to one calendar week of work. A module is structured into a set of lessons. In each lesson, there'll be a set of lecture videos, together with an exercise and a practice quiz to help you make sure you've mastered the material. At the end of each module an assignment in which you'll get to employ the skills you learnt in the module needs to be completed and submitted as per the given instructions. When you complete the course, you'll come away with an in-depth knowledge of front-end JavaScript development using AngularJS and make use of some Web development tools.