'use strict';

angular.module('confusionApp')
        .constant("baseURL","http://localhost:3000/")
        .service('menuFactory',['$resource', 'baseURL', function($resource,baseURL) {
            
            this.getDishes = function(){
                    return $resource(baseURL+"dishes/:id",null,{'update':{method:'PUT'}});
                };
       
            this.getPromotions = function(){
                return $resource(baseURL+ "promotions/:id",null,{'read':{method:'GET'}});
            };
        }])

    .factory('corporateFactory',['$resource', 'baseURL', function($resource,baseURL){
        var corpfac = {};
        corpfac.getLeaders = function(){
            return $resource(baseURL+"leadership/:id",null,{'read':{method:'GET'}});
        };
            return corpfac;
     }])

    .factory('feedBackFactory',['$resource', 'baseURL', function($resource,baseURL){
        var feedfac = {mychannel:"", firstName:"", lastName:"", agree:false, email:""};
        feedfac.getfeedBack = function(){
            return $resource(baseURL+"feedback/:id");
        };
        
        return feedfac;
        
    }]);



