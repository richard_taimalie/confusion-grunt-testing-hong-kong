'use strict';

angular.module('confusionApp')

        .controller('MenuController', ['$scope', 'menuFactory', function($scope, menuFactory) {
            
            $scope.tab = 1;
            $scope.filtText = '';
            $scope.showDetails = false;
            $scope.showMenu = false;
            $scope.message = "Loading...";
                    menuFactory.getDishes().query(
                    function(response){
                        $scope.dishes = response;
                        $scope.showMenu = true;
                    },
                    function(response)  {
                        $scope.message = "Error: " + response.status + " " + response.statusText;
                    });
                
                $scope.select = function(setTab) {
                $scope.tab = setTab;
                
                if (setTab === 2) {
                    $scope.filtText = "appetizer";
                }
                else if (setTab === 3) {
                    $scope.filtText = "mains";
                }
                else if (setTab === 4) {
                    $scope.filtText = "dessert";
                }
                else {
                    $scope.filtText = "";
                }
            };

            $scope.isSelected = function (checkTab) {
                return ($scope.tab === checkTab);
            };
    
            $scope.toggleDetails = function() {
                $scope.showDetails = !$scope.showDetails;
            };
        }])

        .controller('ContactController', ['$scope', function($scope) {

            $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
            
            var channels = [{value:"tel", label:"Tel."}, {value:"Email",label:"Email"}];
            
            $scope.channels = channels;
            $scope.invalidChannelSelection = false;
                        
        }])

        .controller('FeedbackController', ['$scope', 'feedBackFactory', function($scope,feedBackFactory) {
            
            $scope.sendFeedback = function() {
                
                //console.log($scope.feedback);
                
                if ($scope.feedback.agree && ($scope.feedback.mychannel === "")) {
                    $scope.invalidChannelSelection = true;
                    //console.log('incorrect');
                }
                else {
                    $scope.invalidChannelSelection = false;
                    feedBackFactory.getfeedBack().save($scope.feedback);
                    //Reset Form
                    $scope.feedback = {mychannel:"", firstName:"", lastName:"", agree:false, email:"" };
                    $scope.feedback.mychannel="";
                    $scope.feedbackForm.$setPristine();
                    //console.log($scope.feedback);
                }
            };
        }])

        .controller('DishDetailController', ['$scope', '$stateParams', 'menuFactory', function($scope, $stateParams, menuFactory) {

            //$scope.dish = {};
            $scope.showDish = false;
            $scope.message = "Loading ...";
            
            $scope.dish = menuFactory.getDishes().get({id:parseInt($stateParams.id,10)})
                .$promise.then(
                                function(response){
                                    $scope.dish = response;
                                    $scope.showDish = true;
                                },
                                function(response){
                                    $scope.message = "Error: " + response.status + " " + response.statusText;
                                }
            );
            
        }])

       .controller('DishCommentController', ['$scope', 'menuFactory', function($scope,menuFactory) {
            //Create a JavaScript object to hold the comment from the form
            $scope.dishcomment = {rating:5, comment:"", author:"", date:""};
            //console.log($scope.dishcomment);
           
            $scope.submitComment = function () {
                //This is how you record the date
                //"The date property of your JavaScript object holding the comment" = new Date().toISOString();
                $scope.dishcomment.date = new Date().toISOString();
                //console.log($scope.dishcomment);
                //Push your comment into the dish's comment array
                $scope.dish.comments.push($scope.dishcomment);
                menuFactory.getDishes().update({id:$scope.dish.id},$scope.dish);
                //reset your form to pristine
                $scope.commentForm.$setPristine();
                //reset your JavaScript object that holds your comment
                $scope.dishcomment = {rating:5, comment:"", author:"", date:""};
            };
        }])

        // implement the IndexController and About Controller here
        .controller('AboutController',['$scope', '$stateParams', 'corporateFactory', function($scope, $stateParams, corporateFactory){
            
            $scope.showLeaders = "false";
                $scope.message = "Loading ...";
            
            $scope.leaders = corporateFactory.getLeaders().query(
                function(response)  {
                    $scope.leaders = response;
                    $scope.showLeaders = true;
                },
                function(response){
                    $scope.message = "Error: " + response.status + " " + response.statusText;
                });
            
        }])


    .controller('IndexController',['$scope', 'menuFactory', 'corporateFactory', function($scope,  menuFactory, corporateFactory){
        
        //Add staff memeber to front page
       $scope.leader = {};
        $scope.showLeader = false;
        $scope.message = "Loading ...";
        $scope.leader = corporateFactory.getLeaders().get({id:3})
        .$promise.then(
        function(response){
            $scope.leader = response;
            $scope.showLeader = true;
        },
            function(response){
            $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );
        
        //Adds the Promotions to home page
        $scope.promo = {};
        $scope.showPromo = false;
        $scope.message = "Loading ...";
        $scope.promo = menuFactory.getPromotions().get({id:0})
        .$promise.then(
        function(response){
            $scope.promo = response;
            $scope.showPromo = true;
        },
            function(response){
            $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );
        
        //Adds the top dish to the home page
        $scope.dish = {};
        $scope.showDish = false;
        $scope.message = "Loading ...";
        $scope.dish = menuFactory.getDishes().get({id:0})
        .$promise.then(
            function(response){
                $scope.dish = response;
                $scope.showDish = true;
            },
            function(response){
                $scope.message = "Error: " + response.status + " " + response.statusText;
            }
        );
       
 }]);




